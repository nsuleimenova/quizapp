//
//  ViewController.swift
//  QuizApp
//
//  Created by Асем on 01.02.2021.
//

import UIKit

class ViewController: UIViewController {

   
    @IBOutlet weak var result: UILabel!
    
    static var score: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func viewResult(_ sender: UIButton) {
        result.text = "\(ViewController.score)"
    }
    
    
    }
   

